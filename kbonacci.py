'''
Define the k-bonacci as follows. The first k terms are all 1. The nth term, 
for n >= k, is the sum of k previous terms in the sequence. 

For example, the first five terms of the 3-bonacci sequence are 1, 1, 1, 3, 5.
Give n and k, write a function which returns the nth term (0-indexed) of the 
k-bonacci sequence. Since the answer may be quite large, return it as a string.

Example: For k = 3 and n = 4, the output should be kbonacci(k, n) = '5'

Execution time limit 4s (py3)
Input: integer k: 1 <= k <= 10^3
       integer n: 1 <= n <= 25000
'''

from datetime import datetime
start = datetime.now()

def kbonacci_non_iterable(k, n):
    i = n - k
    if k >= i:
        return 1 + (k-1) * 2**(n-k)
    else:
        return "Processing this case..."

def kbonacci(k, n):
    if (1 <= k and k <= 10**3) and (1 <= n and n <= 25000):
        if n < k:
            return 1
        else:
            mylist = []
            for i in range(0, k):
                mylist.append(1)
            for j in range(k, n+1):
                mylist.append(sum(mylist[j-k:j]))
            return mylist[n]
    else:
        return "Invalid input!"

print(kbonacci(3, 4)) # k and n arbitrary
print(datetime.now()-start)                # count execution time