
def check_same_left(a):
    for i in range(0, len(a)-2):
        if a[i] == a[i+1] and a[i+1] == a[i+2]:
            return True
        else:
            return False
def check_same_right(a):
    for i in range(0, len(b)-2):
        if (int(b[i+1][0])-int(b[i][0])==1) and (int(b[i+2][0])-int(b[i+1][0])==1) and (b[i+1][1] == b[i][1]) and (b[i+2][1] == b[i+1][1]):
            return True
        else:
            return False

def cardGroup(s):
    number_list = []
    card_list = []
    for i in range(0, len(s)):
        if i%2 == 0:
            number_list.append(int(s[i]))
    for i in range(0, len(s)-1):
        card_list.append(s[i:i+2])
    while(check_same_left(s)):
        for i in range(0, len(s)-2):
            if s[i] == s[i+1] and s[i+1] == s[i+2]:
                left_point = i
                number = s[i]
                break
        while number in s:
            s.remove(number)
    if len(s) < 3:
        return False
    s = sorted(s)
    while(check_same_right(s)):
        for i in range(0, len(b)-2):
            if (int(b[i+1][0])-int(b[i][0])==1) and (int(b[i+2][0])-int(b[i+1][0])==1) and (b[i+1][1] == b[i][1]) and (b[i+2][1] == b[i+1][1]):
                left_point = i
                break
        for i in range(2, len(b)):
            if (int(b[i][0])-int(b[i-1][0])==1) and (int(b[i-1][0])-int(b[i-2][0])==1) and (b[i][1] == b[i-1][1]) and (b[i-1][1] == b[i-2][1]):
                right_point = i
            else:
                break
    c = [x for x in s if x not in s[left_point:right_point+1]]
    if len(c) != 0:
        return False
    else:
        return True